
/**********************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 2: CPU Scheduler
 * 
 * Class: SchedulerClient
 * Description: Main driver program handling user inputs 
 * 				to set up and run a set of jobs using 
 * 				either RR or SJF scheduling algorithms.
 * 				Data loaded in via a text file.
 **********************************************************
 */

package Client;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import Jobs.PCB;
import Scheduler.RRScheduler;
import Scheduler.SJFScheduler;


public class SchedulerClient {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner console = new Scanner(System.in);
		Scanner inFile = null;
		int menuSelect;
		int timeQuantum = 1;
		int[] cpuBursts;
		int[] ioBursts;
		String fileName;
		String fileLine;
		String[] splitLine;
		ArrayList<PCB> jobs = new ArrayList<PCB>();
		PCB tempPCB;
		
		System.out.println("\n\n   CPU Scheduler\n   Nick Horn\n");
		System.out.print("Select Scheduling Algorithm:\n"
				+ "   1: Shortest Job First (SJF)\n"
				+ "   2: Round Robin\nEnter Selection (1,2): ");
		
		  //Loop to get valid user input for algorithm selection
		while(true) {
			try {
				menuSelect = console.nextInt();
				if(menuSelect >2 || menuSelect < 1) {
					throw new Exception();
				}
				
				break;
			}
			catch(InputMismatchException im) {
				System.out.print("Invalid entry, please retry: ");
				console.next();
			}
			catch(Exception e) {
				System.out.print("Invalid entry, please retry: ");
			}//end try-catch
		}//end while
		
		if(menuSelect == 1) {
			System.out.println("\nShortest Job First Selected.\n");
		}//end menuSelect 1
		
		if(menuSelect == 2) {
			System.out.print("\nRound Robin Selected.\nEnter time quantum length: ");
			
			  //Loop to get valid user entry for time quantum
			while(true) {
				try {
					timeQuantum = console.nextInt();
					System.out.println("RR time quantum set to " + timeQuantum + "\n");
					break;
				}
				catch(InputMismatchException im) {
					System.out.print("Invalid entry, please retry: ");
					console.next();
				}
				catch(Exception e) {
					System.out.print("Invalid entry, please retry: ");
				}//end try-catch
			}//end while
		}//end menuSelect 2
		
		System.out.print("Enter filename of jobs data (.txt will be appended): ");
		
		  //Loop to get valid filename for job data, .txt appended to user entry
		while(true) {
			fileName = console.next();
			try {
				inFile = new Scanner( new FileReader(fileName + ".txt"));
				int pcbID = 1;
				
				  //Loop to build PCB and add to jobs ArrayList
				while(inFile.hasNext()) {
					fileLine = inFile.nextLine();
					splitLine = fileLine.split(" ");
					
					cpuBursts = new int[(splitLine.length / 2) + 1];
					ioBursts = new int[(splitLine.length / 2)];
					
					cpuBursts[0] = Integer.parseInt(splitLine[0]);
					int m = 0;
					int n = 0;
					for(int i = 0; i < splitLine.length; i++) {
						if(i % 2 == 0) {
							cpuBursts[m] = Integer.parseInt(splitLine[i]);
							m++;
						}
						else if (i % 2 == 1) {
							ioBursts[n] = Integer.parseInt(splitLine[i]);
							n++;
						}
					}//end forloop
					
					tempPCB = new PCB(pcbID, cpuBursts, ioBursts);
					jobs.add(tempPCB);
					
					pcbID++;
				}//end inner while
				
				
				break;
			}
			catch(FileNotFoundException fnf) {
				System.out.print("Invalid entry, please retry: ");
			}
			catch(Exception e) {
				System.out.print("Invalid entry, please retry: ");
			}
			
		}//end while
		
		inFile.close();
		
		System.out.println("\nInitial Job Queue:");
		for(PCB p : jobs) {
			System.out.println("\n" + p);
		}
		
		System.out.print("\nRun Starting\n*****************************************\n");
		
		  //initialize and run selected scheduler
		if(menuSelect == 1) {
			SJFScheduler sjf = new SJFScheduler(jobs);
			sjf.runScheduler();
		}
		else if(menuSelect == 2) {
			RRScheduler rr = new RRScheduler(jobs, timeQuantum);
			rr.runScheduler();
		}
		
		System.out.print("\n***Run Complete***\n");
		
	}//end main

}//end class