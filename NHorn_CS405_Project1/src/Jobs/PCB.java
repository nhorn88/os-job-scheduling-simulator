
/*********************************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 1: CPU Scheduler
 * 
 * Class: PCB
 * Description: Simulates a job/process within the system.
 * 			    Data consists of a job ID, a list of CPU bursts,
 * 			    a list of IO bursts and information used to track
 * 				this burst data as a PCB moves through the system.
 *********************************************************************
 */

package Jobs;

import java.util.Arrays;

public class PCB {

	private int jobID;
	private int[] CPUBurstList;
	private int[] IOBurstList;
	private int CBurstIndex;
	private int IBurstIndex;
	private int CBurstRemaining;
	private int IBurstRemaining;
	
	public PCB(int id, int[] CPUBursts, int[] IOBursts) {
		jobID = id;
		CPUBurstList = CPUBursts;
		CBurstRemaining = CPUBurstList[0];
		CBurstIndex = 0;
		IOBurstList = IOBursts;
		IBurstRemaining = IOBurstList[0];
		IBurstIndex = 0;
	}//end constructor
	
	public int getJobID() {
		return jobID;
	}//end getJobID
	
	  //load next CPU burst from CPUBurstList
	public void loadNextCPUBurst() {
		incrementCBurstIndex();
		if(!CPUBurstListIsFinished()) {
			CBurstRemaining = CPUBurstList[CBurstIndex];
		}
	}//loadNextCPUBurst
	
	  //load next IO burst from IOBurstList
	public void loadNextIOBurst() {
		incrementIBurstIndex();
		if(!IOBurstListIsFinished()) {
			IBurstRemaining = IOBurstList[IBurstIndex];
		}
	}//loadNextCPUBurst
	
	public int getCPUBurstRemaining() {
		return CBurstRemaining;
	}//end getCPUBurst
	
	public void setCPUBurstRemaining(int remainingBurst) {
		CBurstRemaining = remainingBurst;
	}//end setCPUBurstRemaining
	
	public int getIOBurstRemaining() {
		return IBurstRemaining;
	}//end getIOBurst
	
	public void setIOBurstRemaining(int remainingBurst) {
		IBurstRemaining = remainingBurst;
	}//end setCPUBurstRemaining
	
	private void incrementCBurstIndex() {
		CBurstIndex++;
	}//end incrementCBurstIndex
	
	private void incrementIBurstIndex() {
		IBurstIndex++;
	}//end incrementIBurstIndex
	
	  //return true if CPUBurstList has been completed, else false
	public boolean CPUBurstListIsFinished() {
		if(CBurstIndex >= CPUBurstList.length) {
			return true;
		}
		return false;
	}//end CPUBurstListIsFinished

	  //return true if IOBurstList has been completed, else false
	public boolean IOBurstListIsFinished() {
		if(IBurstIndex >= IOBurstList.length) {
			return true;
		}
		return false;
	}//end IOBurstListIsFinished
	
	  //return true if both burst lists have been completed, else false
	public boolean isJobFinished() {
		if(CPUBurstListIsFinished() && IOBurstListIsFinished()) {
			return true;
		}
		return false;
	}//end isJobFinished
	
	  //return String containing all information about this PCB
	@Override
	public String toString() {
		String result = "** ID: " + jobID + "\n"
					  + "   CPU Bursts:        " + Arrays.toString(CPUBurstList) + "\n"
					  + "   CPU Burst Index:   " + CBurstIndex + "\n"
					  + "   C-Burst Remaining: " + CBurstRemaining + "\n"
				      + "   IO Bursts:         " + Arrays.toString(IOBurstList) + "\n"
				      + "   IO Burst Index:    " + IBurstIndex + "\n"
				      + "   I-Burst Remaining: " + IBurstRemaining;
		
		return result;
	}//end toString

}//end class