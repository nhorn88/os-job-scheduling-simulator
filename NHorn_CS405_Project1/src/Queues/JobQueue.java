
/************************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 1: CPU Scheduler
 * 
 * Class: JobQueue
 * Description: Simulates a queue of PCBs that are waiting
 * 				to enter the ReadyQueue.  All PCBs are
 * 				initially held here.
 ************************************************************
 */

package Queues;

import java.util.ArrayList;
import Jobs.PCB;

public class JobQueue {

private ArrayList<PCB> queue;
	
	public JobQueue(ArrayList<PCB> jobs) {
		queue = jobs;
	}//end constructor
	
	  //return true if queue is empty, else false
	public boolean isEmpty() {
		if(queue.isEmpty()) {
			return true;
		}
		return false;
	}//end isEmpty
	
	  //return size of queue
	public int getSize() {
		return queue.size();
	}//end getSize
	
	  //remove and return first PCB in queue if it exists, else return null
	public PCB removePCB() {
		if(!queue.isEmpty()) {
		    PCB result = queue.get(0);
			queue.remove(0);
			return result;
		}
		return null;
	}//end removePCB
	
	  //return PCB at this index if it exists, else return null
	public PCB viewPCB(int index) {
		if(index >= 0 && index < queue.size()) {
			return queue.get(index);
		}
		return null;
	}//end viewPCB
	
	  //return String containing jobID's of all PCB in queue
	public String viewJobIDs() {
		String result = "";
		
		for(int i = 0; i < queue.size(); i++) {
			if(queue.get(i) != null) {
				result += queue.get(i).getJobID();
				if(i < queue.size() - 1) {
					result += ", ";
				}
			}
		}
		return result;
	}//end viewJobIDs
	
}//end class