
/**********************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 1: CPU Scheduler
 * 
 * Class: DiskQueue
 * Description: Simulates a queue of PCBs and processes
 * 				their IO bursts.
 **********************************************************
 */

package Queues;

import java.util.ArrayList;
import Jobs.PCB;

public class DiskQueue {

private ArrayList<PCB> queue;
	
	public DiskQueue() {
		queue = new ArrayList<PCB>();
	}//end constructor
	
	  //return true if queue is empty, else false
	public boolean isEmpty() {
		if(queue.isEmpty()) {
			return true;
		}
		return false;
	}//end isEmpty
	
	  //return queue size
	public int getSize() {
		return queue.size();
	}//end getSize
	
	  //add PCB to end of queue
	public void insertPCB(PCB pcb) {
		if(pcb != null) {
			queue.add(pcb);
		}
	}//end insertPCB
	
	  //remove this PCB from queue and return it if it exists, else return null
	public PCB removePCB(PCB pcb) {
		if(queue.contains(pcb)) {
			queue.remove(pcb);
			return pcb;
		}
		return null;
	}//end removePCB
	 
	  //return PCB at this index if it exists, else return null
	public PCB viewPCB(int index) {
		if(index >= 0 && index < queue.size()) {
			return queue.get(index);
		}
		return null;
	}//end viewPCB
	
	  //return String containing jobID's of all PCB in queue
	public String viewJobIDs() {
		String result = "";
		
		for(int i = 0; i < queue.size(); i++) {
			if(queue.get(i) != null) {
				result += queue.get(i).getJobID();
				if(i < queue.size() - 1) {
					result += ", ";
				}
			}
		}
		return result;
	}//end viewJobIDs
	
	  //decrement remaining IO bursts of PCB at given index (if it exists)
	public void updatePCBs(int index) {
		if(index >= 0 && index < queue.size()) {
			int burstRemaining = queue.get(index).getIOBurstRemaining();
			burstRemaining -= 1;
			queue.get(index).setIOBurstRemaining(burstRemaining);
		}
	}//end CPUupdatePCB
	
}//end class