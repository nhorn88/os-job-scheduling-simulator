
/************************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 1: CPU Scheduler
 * 
 * Class: ReadyQueue
 * Description: Simulates a queue of up to three PCBs that
 * 				are ready and waiting for time in the CPU.
 ************************************************************
 */

package Queues;

import java.util.ArrayList;
import Jobs.PCB;

public class ReadyQueue {
	
	private static final int MAX_QUEUE_SIZE = 3;

	private ArrayList<PCB> queue;
	
	public ReadyQueue() {
		queue = new ArrayList<PCB>(MAX_QUEUE_SIZE);
	}//end constructor
	
	  //return true if queue is empty, else false
	public boolean isEmpty() {
		if(queue.isEmpty()) {
			return true;
		}
		return false;
	}//end isEmpty
	
	  //return true if queue is not at max size, else false
	public boolean hasRoom() {
		if(queue.size() < MAX_QUEUE_SIZE) {
			return true;
		}
		return false;
	}//end hasRoom
	
	  //return size of queue
	public int getSize() {
		return queue.size();
	}//end getSize
	
	  //add PCB to end of queue if queue has room
	public void insertPCB(PCB pcb) {
		if(queue.size() < MAX_QUEUE_SIZE && pcb != null) {
			queue.add(pcb);
		}
	}//end insertPCB
	
	  //remove and return PCB at this index if it exists, else return null
	public PCB removePCB(int index) {
		if(index >= 0 && index < queue.size()) {
			PCB pcb = queue.get(index);
			queue.remove(index);
			return pcb;
		}
		return null;
	}//end removePCB
	
	  //return PCB at this index if it exists, else return null
	public PCB viewPCB(int index) {
		if(index >= 0 && index < queue.size()) {
			return queue.get(index);
		}
		return null;
	}//end viewPCB
	
	  //return String containing jobID's of all PCB in queue
	public String viewJobIDs() {
		String result = "";
		
		for(int i = 0; i < queue.size(); i++) {
			if(queue.get(i) != null) {
				result += queue.get(i).getJobID();
				if(i < queue.size() - 1) {
					result += ", ";
				}
			}
		}
		return result;
	}//end viewJobIDs
	
}//end class