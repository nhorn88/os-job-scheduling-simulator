
/************************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 1: CPU Scheduler
 * 
 * Class: IOWaitingQueue
 * Description: Simulates a queue of PCBs that are waiting
 * 				to enter the ReadyQueue.  PCBs enter this
 * 				queue from the DiskQueue after processing.
 ************************************************************
 */

package Queues;

import java.util.ArrayList;
import Jobs.PCB;

public class IOWaitingQueue {

private ArrayList<PCB> queue;
	
	public IOWaitingQueue() {
		queue = new ArrayList<PCB>();
	}//end constructor
	
	  //return true if queue is empty, else false
	public boolean isEmpty() {
		if(queue.isEmpty()) {
			return true;
		}
		return false;
	}//end isEmpty
	
	  //return size of queue
	public int getSize() {
		return queue.size();
	}//end getSize
	
	  //insert PCB to end of queue
	public void insertPCB(PCB pcb) {
		if(pcb != null) {
			queue.add(pcb);
		}
	}//end insertPCB
	
	  //removes and returns first PCB from queue if it exists, else returns null
	public PCB removePCB() {
		if(queue.isEmpty()) {
			return null;
		}
		PCB result = queue.get(0); //FIFO
		queue.remove(result);
		return result;
	}//end removePCB
	
	  //return String containing jobID's of all PCB in queue
	public String viewJobIDs() {
		String result = "";
		
		for(int i = 0; i < queue.size(); i++) {
			if(queue.get(i) != null) {
				result += queue.get(i).getJobID();
				if(i < queue.size() - 1) {
					result += ", ";
				}
			}
		}
		return result;
	}//end viewJobIDs
	
}//end class