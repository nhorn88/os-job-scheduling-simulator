
/***************************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 1: CPU Scheduler
 * 
 * Class: CPU
 * Description: Simulates a simple CPU as a queue of one PCB.
 * 				The CPU processes the PCB's CPU burst.
 ***************************************************************
 */

package Queues;

import Jobs.PCB;

public class CPU {
	
	private PCB pcb;
	
	public CPU() {
		pcb = null;
	}//end constructor
	
	  //return true if CPU is empty
	public boolean isEmpty() {
		if(pcb == null) {
			return true;
		}
		return false;
	}//end isEmpty
	
	  //inserts PCB if CPU empty
	public void insertPCB(PCB pcb) {
		if(this.pcb == null) {
			this.pcb = pcb;
		}
	}//end setPCB
	
	  //removes and returns PCB if one exists, else null
	public PCB removePCB() {
		if(pcb != null) {
			PCB result = pcb;
			pcb = null;
			return result;
		}
		return null;
	}//end removePCB
	
	  //returns PCB if one exists, else null
	public PCB viewPCB() {
		if(pcb != null) {
			return pcb;
		}
		return null;
	}//end viewPCB
	
	  //returns jobID of PCB if one exists, else empty string
	public String viewJobIDs() {
		String result = "";
		if(pcb != null) {
			result += pcb.getJobID();
		}
		return result;
	}//end viewJobIDs
	
	  //decrement remaining CPU burst of PCB (if PCB exists)
	public void updatePCB() {
		if(pcb != null) {
			int burstRemaining = pcb.getCPUBurstRemaining();
			burstRemaining -= 1;
			pcb.setCPUBurstRemaining(burstRemaining);
		}
	}//end CPUupdatePCB

}//end class