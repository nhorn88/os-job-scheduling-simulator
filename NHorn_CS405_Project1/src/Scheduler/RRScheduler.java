
/**********************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 1: CPU Scheduler
 * 
 * Class: RRScheduler
 * Description: CPU Scheduler implemented with a 
 * 				Round Robin scheduling algorithm.
 **********************************************************
 */

package Scheduler;

import java.util.ArrayList;
import Jobs.PCB;
import Queues.CPU;
import Queues.DiskQueue;
import Queues.IOWaitingQueue;
import Queues.JobQueue;
import Queues.ReadyQueue;
import Utilities.SystemClock;

public class RRScheduler {

	private SystemClock clock;
	private JobQueue JQ;
	private ReadyQueue RQ;
	private IOWaitingQueue IQ;
	private CPU cpu;
	private DiskQueue disk;
	private int timeQuantum;
	private int timeRemaining = 0;
	
	public RRScheduler(ArrayList<PCB> jobs, int timeQuantum) {
		clock = new SystemClock();
		JQ = new JobQueue(jobs);
		RQ = new ReadyQueue();
		IQ = new IOWaitingQueue();
		cpu = new CPU();
		disk = new DiskQueue();
		this.timeQuantum = timeQuantum;
	}//end constructor
	
	 //starts the scheduler
	public void runScheduler() {
		ArrayList<PCB> diskTempHolder;
		
		//main loop, runs till all queues are empty
		while(! allJobsFinished()) {
			  //start by filling ReadyQueue
			fillRQ();
			
			  //find shortest job in ReadyQueue and move to CPU
			if(! RQ.isEmpty()) {
				cpu.insertPCB(RQ.removePCB(0));
			}
			
			printLocations();
			timeRemaining = timeQuantum;
			
			  //run current CPU burst and IO bursts
			while(cpu.viewPCB().getCPUBurstRemaining() > 0) {
				if(timeRemaining > 0) {
					tickAndUpdate();
				}
				else {
					break;
				}
			}//end while
			
			if(cpu.viewPCB().getCPUBurstRemaining() <= 0) {
				  //handle cpu burst finished
				cpu.viewPCB().loadNextCPUBurst();
				if(cpu.viewPCB().isJobFinished()) {
					cpu.removePCB(); //job exits system
				}
				else {
					disk.insertPCB(cpu.removePCB());
				}
			}
			else {
				  //cpu burst not finished, send back to ReadyQueue
				RQ.insertPCB(cpu.removePCB());
			}
			
			  //handle io burst finished
			  //find pcb's ready to move
			  //find and move done in two steps to avoid indexes changing during search.
			diskTempHolder = new ArrayList<PCB>();
			for(int i = 0; i < disk.getSize(); i++) {
				if(disk.viewPCB(i).getIOBurstRemaining() <= 0) {
					disk.viewPCB(i).loadNextIOBurst();
					diskTempHolder.add(disk.viewPCB(i));
				}
			}//end for loop
			
			  //move pcb's
			for(int i = 0; i < diskTempHolder.size(); i++) {
				IQ.insertPCB(disk.removePCB(diskTempHolder.get(i)));
			}
			
		}//end main while loop
		
		printLocations();
		
	}//end runScheduler
	
	//fill Ready Queue from IOWait Queue and Job Queue
	private void fillRQ() {
		while(RQ.hasRoom()) {
			if(!IQ.isEmpty()) {
				RQ.insertPCB(IQ.removePCB());
			}
			else if(! JQ.isEmpty()) {
				RQ.insertPCB(JQ.removePCB());
			}
			else {
				break;
			}
		}
	}//end fillRQ
	
	  //update clock and all CPU & Disk PCB's
	private void tickAndUpdate() {
		clock.tick();
		timeRemaining -= 1;
		if(! cpu.isEmpty()) {
			cpu.updatePCB();
		}
		if(! disk.isEmpty()) {
			for(int i = 0; i < disk.getSize(); i++) {
				disk.updatePCBs(i);
			}
		}
	}//end tickAndUpdate
	
	  //print locations of all PCB's listed as ID's in the queues they are currently in
	private void printLocations() {
		System.out.print("\nTime:  " + clock.getTime()
					   + "\n    CPU:          " + cpu.viewJobIDs()
					   + "\n    Job queue:    " + JQ.viewJobIDs()
					   + "\n    Ready queue:  " + RQ.viewJobIDs()
					   + "\n    Disk queue:   " + disk.viewJobIDs()
					   + "\n    IOW queue:    " + IQ.viewJobIDs()
					   + "\n");
	}//end printLocations
	
	  //returns true if all queues empty, else false
	private boolean allJobsFinished() {
		if(JQ.isEmpty() && RQ.isEmpty() && IQ.isEmpty() && cpu.isEmpty() && disk.isEmpty()) {
			return true;
		}
		return false;
	}//end allJobsFinished
	
}//end class