
/**********************************************************
 * Nick Horn
 * CS405: Operating Systems
 * Project 1: CPU Scheduler
 * 
 * Class: SystemClock
 * Description: A simple counter used to simulate 
 * 				time slice ticks within the system.
 **********************************************************
 */

package Utilities;

public class SystemClock {

	private int time;
	
	public SystemClock() {
		time = 0;
	}//end constructor
	
	  //increment time
	public void tick() {
		time++;
	}//end tick
	
	  //return time
	public int getTime() {
		return time;
	}//end getTime
	
}//end class