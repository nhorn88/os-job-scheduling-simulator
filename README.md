# OS Job Scheduling Simulator

A program I created for my OS class.  It is a job scheduler that simulates processes moving through various queues and being processed within a &#34;CPU&#34;.  This program implements both round robin and shortest job first scheduling.